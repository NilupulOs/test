﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MenuHandler : MonoBehaviour {

	public GameObject Osmium;
	public GameObject Splash;
	Animator animator;

	public string movieFileName;
	public Color backgroundColor = Color.black;

	#if UNITY_ANDROID || UNITY_IPHONE
	public FullScreenMovieControlMode controlMod = FullScreenMovieControlMode.Hidden;
	public FullScreenMovieScalingMode scalingMod = FullScreenMovieScalingMode.Fill;
	#endif
	public bool playOnStart = true;

	void Start ()
	{	
		//PlayerPrefs.DeleteAll ();
		StartCoroutine (holdSplash());
		animator = Splash.GetComponent<Animator> ();
	}
	IEnumerator holdSplash ()
	{
		yield return new WaitForSeconds (3);
		showSplash ();
	
	}
	public void showSplash()
	{
		animator.SetInteger ("AnimState",1);
		StartCoroutine (osmium());

	}
	IEnumerator osmium()
	{
		yield return new WaitForSeconds (3);
		playVidio();

	}
	public void loadMain()
	{
		SceneManager.LoadScene (1);
	}
    
	void playVidio()
	{
		Splash.SetActive (false);
		Osmium.SetActive (false);
		print ("play video"+ SaveManager.IsfirstTime);
        
        SaveManager.IsfirstTime=PlayerPrefs.GetInt("IsFirst",0);
		if (string.IsNullOrEmpty (movieFileName))
		{
			Debug.Log("movieFileName is undefined");
			return;
		}
        if (SaveManager.IsfirstTime==0)
		{
            #if UNITY_ANDROID || UNITY_IPHONE
            //Play full screen only
            Handheld.PlayFullScreenMovie ("Intro.mp4",  backgroundColor, controlMod,scalingMod);
			#endif
			SaveManager.IsfirstTime = 1;
			PlayerPrefs.SetInt("IsFirst",1);
		}
		loadMain();

	}

}
