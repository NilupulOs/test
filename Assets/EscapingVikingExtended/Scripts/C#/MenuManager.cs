﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;


enum Level {Cave,Jungle,Village,Empty};

public class MenuManager : MonoBehaviour
{
	private Level gameLevel;
	public GameObject unLockPanel;
	public GameObject mapScreen;
	public GameObject menuScreen;
	public GameObject shopScreen;
	public GameObject creditScreen;
	public GameObject comingSoonPanel;
    public GameObject menuBack;
    public GameObject menuStory;
    public GameObject shopErrorPanel;
    public GameObject shopConformPanel;
    public GameObject InternetErrorPanel;
    public Text[] ShopOwnedItems;
	public Sprite[] audioSprites;


    //Shop Powerup Animation
    bool playSpeed;
    bool playSheld;
    bool playBlast;
    bool playHealth;

    public GameObject startpositionSpeed;
    public GameObject startpositionSP;
    public GameObject endpositionSpeed;
    public GameObject startpositionShield;
    public GameObject startpositionSH;
    public GameObject endpositionShield;
    public GameObject startpositionBlast;
    public GameObject startpositionBL;
    public GameObject endpositionBlast;
    public GameObject startpositionHealth;
    public GameObject startpositionHE;
    public GameObject endpositionHealth;

    public GameObject SpeedIcon;
    public GameObject ShieldIcon;
    public GameObject BlastIcon;
    public GameObject HealthIcon;



    public GameObject Jungle;
	public GameObject Hills;
	public GameObject AudioButton;

	public Sprite[] levelImages;
    int buttonID;
	public Text Price;

	public AudioSource musicPlayer;
	public bool audioEnabled;
    // Use this for initialization

    public Color backgroundColor = Color.black;

    #if UNITY_ANDROID || UNITY_IPHONE
        public FullScreenMovieControlMode controlMod = FullScreenMovieControlMode.Hidden;
        public FullScreenMovieScalingMode scalingMod = FullScreenMovieScalingMode.Fill;
    #endif
    public bool playOnStart = true;

    void Start ()
	{
        SpeedIcon.SetActive(false);
        ShieldIcon.SetActive(false);
        BlastIcon.SetActive(false);
        HealthIcon.SetActive(false);
        print ("SaveManager.audioEnabled "+SaveManager.audioEnabled);
		if (SaveManager.audioEnabled == 1)
		{
			audioEnabled = true;

			if (musicPlayer)
				musicPlayer.Play();
		}
		else
		{
			audioEnabled = false;
		}
		AudioButton.GetComponent<Image>().sprite = audioEnabled == true ? audioSprites[0] : audioSprites[1];
		//PlayerPrefs.DeleteAll ();
		gameLevel = Level.Empty;

		if (GameManager.IsShop)
		{
			GameManager.IsShop = false;
			ShopClicked ();
		}
		SaveManager.level2Unlocked=PlayerPrefs.GetInt("level2Unlocked",0);
		if (SaveManager.level2Unlocked==1)
		{
			Jungle.GetComponent<Image>().sprite=levelImages[0];
		}
		SaveManager.level2Unlocked=PlayerPrefs.GetInt("level3Unlocked",0);
		if (SaveManager.level3Unlocked==1)
		{
			Hills.GetComponent<Image>().sprite=levelImages[1];
		}
        SaveManager.LoadData();
    }
	void Update ()
    {
        if (playSpeed)
        {
            SpeedIcon.transform.position = Vector3.Lerp(startpositionSpeed.transform.position, endpositionSpeed.transform.position, 3f * Time.deltaTime);
            StartCoroutine(stopMoveSpeed());
        }
        if (playSheld)
        {
            ShieldIcon.transform.position = Vector3.Lerp(startpositionShield.transform.position, endpositionShield.transform.position, 3f * Time.deltaTime);
            StartCoroutine(stopMoveSheild());
        }
        if (playBlast)
        {
            BlastIcon.transform.position = Vector3.Lerp(startpositionBlast.transform.position, endpositionBlast.transform.position, 3f * Time.deltaTime);
            StartCoroutine(stopMoveBlast());
        }
        if (playHealth)
        {
            HealthIcon.transform.position = Vector3.Lerp(startpositionHealth.transform.position, endpositionHealth.transform.position, 3f * Time.deltaTime);
            StartCoroutine(stopMoveHeal());
        }
    }
    IEnumerator stopMoveSpeed()
    {
        yield return new WaitForSeconds(1);
        playSpeed = false;
        SpeedIcon.SetActive(false);
        SpeedIcon.transform.position = startpositionSP.transform.position;
    }

    IEnumerator stopMoveSheild()
    {
        yield return new WaitForSeconds(1);
        playSheld = false;
        ShieldIcon.SetActive(false);
        ShieldIcon.transform.position = startpositionSH.transform.position; 
    }

    IEnumerator stopMoveBlast()
    {
        yield return new WaitForSeconds(1);
        playBlast = false;
        BlastIcon.SetActive(false);
        BlastIcon.transform.position = startpositionBL.transform.position;
    }
    IEnumerator stopMoveHeal()
    {
        yield return new WaitForSeconds(1);
        playHealth = false;
        HealthIcon.SetActive(false);
        HealthIcon.transform.position = startpositionHE.transform.position;
    }
    public void playVideo()
    {
        #if UNITY_ANDROID || UNITY_IPHONE
        //Play full screen only
        Handheld.PlayFullScreenMovie("Intro.mp4", backgroundColor, controlMod, scalingMod);
        #endif
    }
    public void mapBackClicked()
    {
        menuScreen.SetActive(true);
        mapScreen.SetActive(false);
    }
	public void unLockClose()
	{
		unLockPanel.SetActive (false);
	}
	public void comingSoonPanelClicked()
	{
        menuBack.SetActive(false);
        menuStory.SetActive(false);
        comingSoonPanel.SetActive(true);
	}
	public void comingSoonbackClicked()
	{
		comingSoonPanel.SetActive(false);
        menuBack.SetActive(true);
        menuStory.SetActive(true);
    }	
	public void unLockPanelOpen(int id)
	{
		gameLevel = Level.Empty;
		if(id==1)
		{
			SceneManager.LoadScene ("Cave");
		}
		else if(id==2)
		{

			SaveManager.level2Unlocked=PlayerPrefs.GetInt("level2Unlocked",0);
			if (SaveManager.level2Unlocked==1)
			{
				print ("Level one Unlocked");
				SceneManager.LoadScene ("Jungle");
			}
			else
			{
				unLockPanel.transform.GetChild(0).GetChild(1).GetComponent<Text>().text="35000";
				gameLevel = Level.Jungle;
				unLockPanel.SetActive (true);
			}
		}
		else if(id ==3)
		{
			if (GameManager.level3Unlocked)
			{
				print ("Level three Unlocked");
			}
			else
			{
				unLockPanel.transform.GetChild(0).GetChild(1).GetComponent<Text>().text="45000";
				gameLevel = Level.Village;
				unLockPanel.SetActive (true);
			}
		}
	}
	public void unLockClicked()
	{
		if (gameLevel == Level.Jungle)
		{
			if(SaveManager.coinAmmount - 100 >= 0)
			{
				SaveManager.coinAmmount -= 100;
				SaveManager.level2Unlocked=1;
				PlayerPrefs.SetInt("level2Unlocked",1);
				Jungle.GetComponent<Image>().sprite=levelImages[0];
				//print("aaa"+levelImages[0].name);
				updateInfo();
			}
			else
			{
				
			}

		}
		if (gameLevel == Level.Village)
		{
			GameManager.level2Unlocked = true;
		}
		unLockClose ();
	}
	public void skipClicked()
	{
		menuScreen.SetActive (true);
		//Load the selected level
	}
	public void playClicked()
	{
		menuScreen.SetActive (false);
		mapScreen.SetActive (true);
	}
	IEnumerator loadMap()
	{
		yield return new WaitForSeconds (0);
		skipClicked ();
	}
	public void ShopClicked()
	{
		menuScreen.SetActive (false);
		shopScreen.SetActive (true);        
        updateInfo ();
	}
    public void linkClicked()
    {
        Application.OpenURL("http://osmium.com.au/");
    }
    public void ShopCloseClicked()
	{
		menuScreen.SetActive (true);
		shopScreen.SetActive (false);
	}
    public void ShowConfamationMsg(int buttonId)
    {
        shopConformPanel.SetActive(true);
        buttonID = buttonId;
    }
    public void buyPowerups()
    {
        shopConformPanel.SetActive(false);
        if (buttonID == 1)
        {
            playSpeed = true;
            SpeedIcon.SetActive(true);
            SaveManager.coinAmmount -= 2000;
            SaveManager.extraSpeed += 1;
            SaveManager.SaveData();
            //Notify mission manager, and update shop display
            updateInfo();
        }
        else if (buttonID == 2)
        {
            playSheld = true;
            ShieldIcon.SetActive(true);
            SaveManager.coinAmmount -= 2000;
            SaveManager.shield += 1;
            SaveManager.SaveData();
            //Notify mission manager, and update shop display
            updateInfo();
        }
        else if (buttonID == 3)
        {
            playBlast = true;
            BlastIcon.SetActive(true);
            SaveManager.coinAmmount -= 2000;
            SaveManager.sonicWave += 1;
            SaveManager.SaveData();

            //Notify mission manager, and update shop display
            updateInfo();
        }
        else if (buttonID == 4)
        {
            playHealth = true;
            HealthIcon.SetActive(true);
            SaveManager.coinAmmount -= 2000;
            SaveManager.revive += 1;
            SaveManager.SaveData();

            //Notify mission manager, and update shop display
            updateInfo();
        }
        buttonID = 0;
    }
	public void buySpeed()
	{
        if (SaveManager.coinAmmount - 2000 >= 0)
        {
            ShowConfamationMsg(1);            
        }
        else if(2000 > SaveManager.coinAmmount)
        {
            shopErrorPanel.SetActive(true);
        }
	}
	public void BuyShield()
	{
		if (SaveManager.coinAmmount - 2000 >= 0)
		{
            ShowConfamationMsg(2);            
		}
        else if(2000 > SaveManager.coinAmmount)
        {
            shopErrorPanel.SetActive(true);
        }
    }
	//Called, when the player buys a sonic blast powerup
	public void BuySonicBlast()
	{
		if (SaveManager.coinAmmount - 2000 >= 0)
		{
            ShowConfamationMsg(3);            
		}
        else if (2000 > SaveManager.coinAmmount)
        {
            shopErrorPanel.SetActive(true);
        }
    }
	//Called, when the player buys a revive powerup
	public void BuyRevive()
	{
		if (SaveManager.coinAmmount - 2000 >= 0)
		{
            ShowConfamationMsg(4);
        }
        else if (2000 > SaveManager.coinAmmount)
        {
            shopErrorPanel.SetActive(true);
        }
    }

	void updateInfo()
	{
        SaveManager.LoadData();
        //print("aaaaaaaaaaa"+ SaveManager.coinAmmount); 

        Price.text =SaveManager.coinAmmount.ToString();
        ShopOwnedItems[0].text = SaveManager.extraSpeed.ToString();
		ShopOwnedItems[1].text = SaveManager.shield.ToString();
		ShopOwnedItems[2].text = SaveManager.sonicWave.ToString();
		ShopOwnedItems[3].text = SaveManager.revive.ToString();
	}
	public void creditsClick()
	{		
		menuScreen.SetActive (false);
		creditScreen.SetActive (true);
	}
	public void creditsBackClick()
	{
		creditScreen.SetActive (false);
		menuScreen.SetActive (true);
	}
	public void quitClicked()
	{
		Application.Quit ();
	}
	public void buyGold()
	{
        shopErrorPanel.SetActive(false);
        print("cccccc");
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            Debug.Log("Error. Check internet connection!");
            InternetErrorPanel.SetActive(true);
        }
        else
        {
            Debug.Log("internet connection Sucess!");
            PurchaseManager.Instance.Buy5000Gold();
            updateInfo();
        }
        
	}
	public void ChangeAudioState()
	{
		if (audioEnabled)
		{
			audioEnabled = false;
			SaveManager.audioEnabled = 0;

			if (musicPlayer)
				musicPlayer.Stop();
		}
		else
		{
			audioEnabled = true;
			SaveManager.audioEnabled = 1;

			if (musicPlayer)
				musicPlayer.Play();
		}
		print ("SaveManager.audioEnabled2 "+SaveManager.audioEnabled);
		SaveManager.SaveData();
		AudioButton.GetComponent<Image>().sprite = audioEnabled == true ? audioSprites[0] : audioSprites[1];
		print ("SaveManager.audioEnabled3 "+SaveManager.audioEnabled);
	}
    public void errorPanelDisable()
    {
        shopErrorPanel.SetActive(false);
        shopConformPanel.SetActive(false);
    }
    public void disableIntenetErrorMsg()
    {
        InternetErrorPanel.SetActive(false);
    }

}
