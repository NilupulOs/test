﻿using UnityEngine;
using UnityEngine.Advertisements;
using System.Collections;

public class AdsManager : MonoBehaviour {


    public GameObject noadsPanel;
    public void Start()
    {
        noadsPanel.SetActive(false);
    }
	public void showAds()
	{
        if (Advertisement.IsReady("rewardedVideo"))
        {
            Debug.Log("Ads ready");
            //Advertisement.Show ();
            Advertisement.Show("rewardedVideo", new ShowOptions() { resultCallback = HandeldAdResults });
        }
        else
        {
            Debug.Log("Ads not ready");
            noadsPanel.SetActive(true);
        }
	}
	private void HandeldAdResults(ShowResult result)
	{
		switch (result)
		{
			case ShowResult.Finished:
				GUIManager.Instance.RevivePlayer ();
				GUIManager.Instance.watchedVideo ();
				break;
			case ShowResult.Skipped:
				Debug.Log ("Player Skiped the vidio");
				break;
		}

	}
}
