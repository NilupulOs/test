﻿using UnityEngine;
using Facebook.Unity;
using UnityEngine.UI;

public class FBShare : MonoBehaviour
{
    private void Awake()
    {
        //PlayerPrefs.DeleteAll();
        if (!FB.IsInitialized)
        {
            FB.Init();
        }
        else
        {
            FB.ActivateApp();
        }
    }
    public void LogIn()
    {
        FB.LogInWithReadPermissions(callback:OnLogIn);
    }
    private void OnLogIn(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
            AccessToken token = AccessToken.CurrentAccessToken;
        }
        else
        {
            Debug.Log("Loging Canceled");
        }
    }
    public void Share()
    {
        FB.FeedShare(
                link: new System.Uri("http://escapingviking.com/game"),
                linkName: "Escaping Viking",
                linkCaption: "",
                linkDescription: "Look at my best score: "+SaveManager.bestDistance+" on Escaping Viking - can you beat me??",
                // linkDescription: "Look at my best score: " + GameControll.HighScore + " on Zen Trap - can you beat me??",
                picture: new System.Uri("https://fb-s-b-a.akamaihd.net/h-ak-xap1/t39.2081-0/p128x128/15292498_351648635204090_3609600445246341120_n.png"),
                callback: OnShare);
        //FB.ShareLink(
        //    contentTitle:"AAAAA",
        //    contentDescription:"bbbbbbbb",
        //    photoURL:new System.Uri( "https://www.facebook.com/EscapingViking/photos/a.1230235887047562.1073741825.1230207967050354/1236927496378401/?type=1&theater"),
           // callback: OnShare );
    }
    public void OnShare(IShareResult result)
    {
        if (result.Cancelled || !string.IsNullOrEmpty(result.Error))
        {
            Debug.Log("Share Link Error " + result.Error);
        }
        else if (!string.IsNullOrEmpty(result.PostId))
        {
            Debug.Log(result.PostId);
        }
        else
        {
            Debug.Log("Success");
        }
    }

}
