﻿using UnityEngine;
using System.Collections;

public class ChainMove : MonoBehaviour {

	public GameObject chain1;
	public GameObject chain2;
	bool isGoingup;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		
		if(chain1.transform.position.y>3)
		{
			isGoingup = false;
		}
		else if(chain1.transform.position.y<-3)
		{
			isGoingup = true;
		}
		if (isGoingup)
		{
			chain1.transform.Translate (Vector3.up * Time.deltaTime * 1);
			chain2.transform.Translate (Vector3.up * Time.deltaTime * -1);
		}
		else
		{

			chain1.transform.Translate (Vector3.up*Time.deltaTime*-1);
			chain2.transform.Translate (Vector3.up*Time.deltaTime*1);
		}
	}
}
